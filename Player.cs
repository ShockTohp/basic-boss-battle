using Godot;
using System;

public class Player : KinematicBody2D
{
	Vector2 velocity = new Vector2();
	Vector2 direction = new Vector2();
	
	public void ReadInput() {
		velocity = new Vector2();

		if(Input.IsKeyPressed((int)KeyList.Up)) {
			velocity.y -= 1;
			direction = new Vector2(0, -1);
		}
		if(Input.IsKeyPressed((int)KeyList.Down)) {
			velocity.y += 1;
			direction = new Vector2(0, 1);
		}
		if(Input.IsKeyPressed((int)KeyList.Left)) {
			velocity.x -= 1;
			direction = new Vector2(-1, 0);
		}
		if(Input.IsKeyPressed((int)KeyList.Right)) {
			velocity.x += 1;
			direction = new Vector2(1, 0);
		}

		velocity = velocity.Normalized();
		velocity = MoveAndSlide(velocity * 200);
		Rotation = direction.Angle();
	}
	//Executed Everyframe
	//GAME LOOP . JPEG
	public override void _Process(float delta) {
		ReadInput();
		// face the mouse
	}
}
